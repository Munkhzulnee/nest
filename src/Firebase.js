import 'firebase/firestore';
import 'firebase/functions';

import firebase from 'firebase/app';

let app;
if (!firebase.apps.length) {
    app = firebase.initializeApp({
        apiKey: "AIzaSyBbyrkggV6PvMdOqh2BCOaHlfdCwSqu0Gc",
        authDomain: "nest-school.firebaseapp.com",
        databaseURL: "https://nest-school.firebaseio.com",
        projectId: "nest-school",
        storageBucket: "nest-school.appspot.com",
        messagingSenderId: "1072669390949",
        appId: "1:1072669390949:web:96ab12ab22727bca"
    });
}

const firestore = firebase.firestore(app);
const functions = firebase.functions(app);

export { firestore, functions };
