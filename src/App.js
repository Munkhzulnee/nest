import React from 'react';
import './App.css';
import Profile from './pages/each-students/Profile';
import Students from './pages/students/Students';
import { Switch, Route } from 'react-router-dom';
import Home from './pages/Home'

function App() {
    return (
        <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/students" exact component={Students} />
            <Route path="/student/:nestId" exact component={Profile} />
        </Switch>
    );
}

export default App;
