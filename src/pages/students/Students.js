import React, { useState, useEffect } from 'react';
import { collectionData } from 'rxfire/firestore';
import { firestore } from '../../Firebase';
import { Link } from 'react-router-dom';
const Students = () => {
    
    const [students, setStudents] = useState([]);

    useEffect(() => {
        const ref = firestore.collection('Students');
        collectionData(ref).subscribe(data => {
            console.log(data);
            const firstNames = data.map(student => {
                return {
                    name: student.firstName,
                    id: student.nestId
                };
            })
            
            setStudents(firstNames);
            console.log(data);
        })
    },[])

    return (
        <div>
            {students.map(student => {
                return (
                    <div>
                        <h1>{student.name} </h1>
                        <Link to={'/student/' + student.id}>{student.id}</Link>
                    </div>
                )
            })}

        </div>
    )
}

export default Students;