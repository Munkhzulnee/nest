import React from "react";
import "./Profile.css";
import OsChart from "./charts/os-chart";
import DesignChart from "./charts/design-chart";
import AttendanceChart from "./charts/attendance-chart";
import { useState, useEffect } from "react";
import { docData } from "rxfire/firestore";
import { firestore } from "../../Firebase";
import * as _ from "lodash";

const Profile = ({ match }) => {
  function updateStudent() {
    console.log("updated");
  }
  const [student, setStudent] = useState([]);
  useEffect(() => {
    const ref = firestore.doc(`Students/${match.params.nestId}`);
    docData(ref).subscribe(data => {
      setStudent(data);
    });
  }, []);

  return (
    <div className="all">
      <div className="header">
        <div className="profile-picture">
          <img src={_.get(student, "picture[0].url")} alt=""></img>
        </div>
        <div className="about">
          <table id="information">
            <tr>
              <td>Сурагчийн овог:</td>
              <td>{student.lastName}</td>
            </tr>
            <tr>
              <td>Сурагчийн нэр:</td>
              <td>{student.firstName}</td>
            </tr>
            <tr>
              <td>Сурагчийн код:</td>
              <td>{student.nestId}</td>
            </tr>
            <tr>
              <td>Сурагчийн утас:</td>
              <td>{student.studentPhoneNumber}</td>
            </tr>
            <tr>
              <td>Эцэг эхийн утас:</td>
              <td>{student.parentsPhoneNumber}</td>
            </tr>
            <tr>
              <td>Сурагчийн ээлж:</td>
              <td>{student.school}</td>
            </tr>
            <tr>
              <td>Сурагчийн анги:</td>
              <td>{student.grade}</td>
            </tr>
          </table>
        </div>
      </div>
      <div className="main">
        <h1>DATE</h1>
        <table id="process">
          <tr>
            <td>Ирц</td>
            <td>Өдөр</td>
            <td>Цаг</td>
            <td>
              Үйлдлийн системийн <br /> үндэс, Програмчлалын <br />
              үндэс
            </td>
            <td>Гүйцэтгэл</td>
            <td>
              Дизайны <br /> үндэс
            </td>
            <td>Гүйцэтгэл</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </table>
      </div>
      <div className="footer">
        <div className="attendance-chart">
          <AttendanceChart />
        </div>
        <div className="os-chart">
          <OsChart></OsChart>
        </div>
        <div className="design-chart">
          <DesignChart />
        </div>
      </div>
      <button onClick={updateStudent}>update</button>
    </div>
  );
};

export default Profile;
