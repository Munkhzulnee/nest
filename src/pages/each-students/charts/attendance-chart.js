import React, {Component} from 'react';
import { Pie } from 'react-chartjs-2';
import ChartDataLabels from 'chartjs-plugin-datalabels';

const data = {
  
  labels: ['Ирсэн + Хоцорсон', 'Чөлөөтэй', 'Тасалсан', 'Өвчтэй'],
  datasets: [
    {
      data: [30, 0, 1, 0],
      backgroundColor: [
        '#0CD9C5',
        '#36A2EB',
        '#FFCE56',
        '#FA775B'
        ]
    }
    
  ]
};

const options= {
  title:{
    display: true,
    text: 'Stundent Attendance'
  },
  plugins:{
    datalabels:{
      color: 'black',
      display: true
    }
  }
};
export default class AttendanceChart extends Component {
  render() {
    return (
      <div>
        <Pie ref="chart" data={data} options={options} plugins={ChartDataLabels}
        height={370} width={220}/>
      </div>
    );
  }

  
}