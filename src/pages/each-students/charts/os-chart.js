import React, {Component} from 'react';
import { Bar} from 'react-chartjs-2';
import ChartDataLabels from 'chartjs-plugin-datalabels';

const data={
    labels: ['Бодсон', 'Бодоогүй'],
    datasets: [{
        data: [32, 43],
        backgroundColor:[
            '#F2CD21',
            '#FA775B'
        ]
    }]

};
const options={
    scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            min: 0
          }    
        }]
      },
      title : {
        display: true,
        text: 'Үйлдлийн системийн үндэс, Програмчлалын үндэс'
      },
      plugins:{
        datalabels:{
          color: 'black',
          display: true
        }
      },
      legend: {
        display: false
      }
}

export default class oschart extends Component{
    render() {
        return (
          <div>
            <Bar ref="chart" data={data} options={options} plugins={ChartDataLabels} 
            height={370} width={220}/>
          </div>
        );
    }
}