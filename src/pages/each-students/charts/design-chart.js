import React, {Component} from 'react';
import {Bar} from 'react-chartjs-2';

const data = {
    labels:['Гүйцэтгэсэн','Гүйцэтгээгүй'],
    datasets:[{
        data:[1,4],
        backgroundColor:[
            '#FA775B',
            '#0CD9C5'
        ]
    }]
};
const options= {
    scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            min: 0,
            max: 5
          }    
        }]
      },
    title:{
        display: true,
        text:'Дизайны үндэс'
    },
    plugins:{
        datalabels:{
            color: 'black'
        }
    },
    legend:{
        display: false
    }
};

export default class DesignChart extends Component{
    render(){
        return(
            <Bar data={data} options={options} height={370} width={220}/>
        )
    }
} 