import React from 'react';
import { Link } from 'react-router-dom';
import { functions } from '../Firebase';
import { useState } from 'react';

const Home = () => {
    const [hmn, setHmn]= useState(false);
    const fetchStudents = async () => { 
        setHmn(true);
        const fetchStudentsFn = functions.httpsCallable('fetchStudents');
        await fetchStudentsFn({});
        setHmn(false);
    }

    return (
        <div>
            <Link to='/students'>
                <button>Сурагчдын нэрс</button>
            </Link> <br />
            <button onClick={fetchStudents} disabled={hmn}> Мэдээллийг шинчлэх</button>
        </div>
    )
}

export default Home;
