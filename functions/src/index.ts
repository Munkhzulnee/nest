import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { SSL_OP_NO_QUERY_MTU } from 'constants';
import { mixedTypeAnnotation, exportDefaultDeclaration } from '@babel/types';
const Airtable = require('airtable');

var serviceAccount = require("./nest-school-firebase-adminsdk-l50iz-b4bbe24d4b.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://nest-school.firebaseio.com"
});

const base = new Airtable({ apiKey: 'keyJFnayvzGLecrfr' }).base('appfOPDFLNzrKOOIa');

export const fetchStudents = functions.https.onCall(async () => {
    base('Students').select({
        view: "Grid view"
    }).eachPage(async (records, fetchNextPage) => {

        await Promise.all(
            records.map(async (record)  => {
                let data = {
                    firstName: record.get('Нэр'),
                    lastName: record.get('Овог'),
                    school: record.get('Ээлж'),
                    grade: record.get('Анги'),
                    nestId: record.get('Student ID'),
                    studentPhoneNumber: record.get('Өөрийн дугаар'),
                    parentsPhoneNumber: record.get('ЭЭАХ-ын дугаар'),
                    picture: record.get('Зураг')
                }
                data = JSON.parse(JSON.stringify(data));
                const id = record.get('Student ID');

                return admin.firestore().doc(`Students/${id}`).set(data);
            })
        );

        fetchNextPage();

    }, function done(err) {
        if (err) { console.error(err); return; }
    });

    return true;
});
